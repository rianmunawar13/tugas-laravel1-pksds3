<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('form');
    }

    public function welcome(Request $request)
    {
        $last_name = $request['last-name'];
        $first_name = $request['first-name'];

        return view('welcome', compact('last_name', 'first_name'));
    }
}
