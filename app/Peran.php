<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peran extends Model
{
    protected $guarded = [];

    public function cast()
    {
        return $this->belongsTo(Cast::class);
    }

    public function peran()
    {
        return $this->belongsTo(Peran::class);
    }
}
