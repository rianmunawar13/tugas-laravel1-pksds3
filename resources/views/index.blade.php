@extends('layouts.index')

@section('title')
    Halaman Home
@endsection

@section('content')
    <h1>Media Online</h1>

    <h3>Social Media Developer</h3>
    <p>Belajar dan berbagi agar hidup lebih baik</p>

    <h3>Benefit Join di Media Online</h3>
    <ul>
        <li>Mendapat motivasi sesama para Developer</li>
        <li>Sharing knowledge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>

    <h3>Cara Bergabung ke Media Online</h3>
    <ol>
        <li>Mengunjungi website ini</li>
        <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
@endsection
