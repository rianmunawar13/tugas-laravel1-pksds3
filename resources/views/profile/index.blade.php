@extends('layouts.index')

@section('title')
    Halaman Update Profile
@endsection

@section('content')
    <form method="POST" action="/profile/{{ $profile->id }}">
        @csrf
        @method('PUT')
        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                    autocomplete="name" value="{{ $profile->user->name }}" disabled>

                @error('name')
                    <span class="alert alert-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-mail Address') }}</label>

            <div class="col-md-6">
                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror"
                    email="email" autocomplete="email" value="{{ $profile->user->email }}" disabled>

                @error('email')
                    <span class="alert alert-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="umur" class="col-md-4 col-form-label text-md-right">{{ __('Umur') }}</label>

            <div class="col-md-6">
                <input id="umur" type="number" class="form-control @error('umur') is-invalid @enderror" name="umur"
                    autocomplete="umur" value="{{ $profile->umur }}">

                @error('umur')
                    <span class="alert alert-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="bio" class="col-md-4 col-form-label text-md-right">{{ __('Bio') }}</label>

            <div class="col-md-6">
                <textarea id="bio" class="form-control @error('bio') is-invalid @enderror" name="bio" autocomplete="bio">{{ $profile->bio }}</textarea>

                @error('bio')
                    <span class="alert alert-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="alamat" class="col-md-4 col-form-label text-md-right">{{ __('Alamat') }}</label>

            <div class="col-md-6">
                <textarea id="alamat" class="form-control @error('alamat') is-invalid @enderror" name="alamat"
                    autocomplete="alamat">{{ $profile->alamat }}</textarea>

                @error('alamat')
                    <span class="alert alert-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Update') }}
                </button>
            </div>
        </div>
    </form>
@endsection
