@extends('layouts.index')

@section('title')
    Welcome page
@endsection

@section('content')
    <h1>SELAMAT DATANG {{ $first_name . ' ' . $last_name }} !</h1>
    <strong>
        <p>
            Terima kasih telah bergabung di Website Kami. Media Belajar kita
            bersama!
        </p>
    </strong>
@endsection
