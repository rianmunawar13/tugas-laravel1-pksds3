@extends('layouts.index')

@section('title')
    Form Register
@endsection

@section('content')
    <h1>Buat Akun Baru</h1>

    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <div>
            <label for="first-name">First Name :</label> <br />
            <input type="text" name="first-name" required />
        </div>
        <br />
        <div>
            <label for="last-name">Last Name :</label> <br />
            <input type="text" name="last-name" required />
        </div>
        <br />
        <div>
            <p>Gender</p>
            <input type="radio" name="gender" value="male" />
            <label for="gender">Male</label> <br />
            <input type="radio" name="gender" value="female" />
            <label for="gender">Female</label>
        </div>
        <br />
        <div>
            <p>Nationality</p>
            <select name="nationality" id="nationality">
                <option value="id">Indonesia</option>
                <option value="eng">English</option>
                <option value="as">Amerika</option>
                <option value="sg">Singapura</option>
            </select>
        </div>
        <div>
            <p>Language Spoken</p>
            <input type="checkbox" name="id" />
            <label for="id">Bahasa Indonesia</label> <br />
            <input type="checkbox" name="eng" />
            <label for="eng">English</label> <br />
            <input type="checkbox" name="eng" />
            <label for="eng">Other</label>
        </div>
        <br />
        <div>
            <p>Bio</p>
            <textarea name="bio" id="input-bio" cols="30" rows="10"></textarea>
        </div>
        <button type="submit">SignUp</button>
    </form>
@endsection
