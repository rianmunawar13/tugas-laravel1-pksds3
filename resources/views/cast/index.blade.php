@extends('layouts.index')

@section('title')
    All Caster
@endsection

@section('content')
    @auth
        <a href="/cast/create" class="btn btn-primary mb-3">Tambah Caster</a>
    @endauth

    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($casts as $key=>$cast)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $cast->nama }}</td>
                    <td>{{ $cast->umur }}</td>
                    <td>{{ $cast->bio }}</td>
                    <td>
                        @auth
                            <form action="/cast/{{ $cast->id }}" method="post">
                                <a href="/cast/{{ $cast->id }}" class="btn btn-info btn-sm">Detail</a>
                                <a href="/cast/{{ $cast->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                                @csrf
                                @method('delete')
                                <input type="submit" name="btn-submit" class="btn btn-danger btn-sm" value="Delete">
                            </form>
                        @endauth

                        @guest
                            <a href="/cast/{{ $cast->id }}" class="btn btn-info btn-sm">Detail</a>
                        @endguest
                    </td>
                </tr>
            @empty
                <h1>Data Kosong</h1>
            @endforelse
        </tbody>
    </table>
@endsection
