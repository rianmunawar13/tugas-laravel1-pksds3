@extends('layouts.index')

@section('title')
    Detail Caster
@endsection

@section('content')
    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h1>{{ $cast->nama }}</h1>
            <h4 class="text-muted">{{ $cast->umur }}</h4>
            <p class="card-text">{{ $cast->bio }}</p>
            <a href="/cast" class="btn btn-primary">Go Back</a>
            @auth
                <a href="/cast/{{ $cast->id }}/edit" class="btn btn-warning">Edit</a>
            @endauth
        </div>
    </div>
@endsection
